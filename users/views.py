from django.shortcuts import render, redirect
# from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from django.contrib.auth.decorators import login_required


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(
                request, f'Your account has been created! You are now able to log in')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})


@login_required
def profile(request):
    if request.method == 'POST':

        user_UpdateForm = UserUpdateForm(request.POST, instance=request.user)
        profile_UpdateForm = ProfileUpdateForm(request.POST,
                                               request.FILES,
                                               instance=request.user.profile)
        if(user_UpdateForm.is_valid() and profile_UpdateForm.is_valid()):
            user_UpdateForm.save()
            profile_UpdateForm.save()
            messages.success(
                request, f'Your account has been updated!')
            return redirect('profile')

    else:
        user_UpdateForm = UserUpdateForm(instance=request.user)
        profile_UpdateForm = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'user_UpdateForm': user_UpdateForm,
        'profile_UpdateForm': profile_UpdateForm
    }

    return render(request, 'users/profile.html', context)
